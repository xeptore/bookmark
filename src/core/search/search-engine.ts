export interface SearchEngine<T> {
  readonly search: (term: string) => ReadonlySet<T>;
  readonly setDocs: (docs: readonly T[]) => void;
}

export interface InternalSearchEngine<T> {
  readonly search: (term: string) => T[];
  readonly setDocs: (docs: readonly T[]) => void;
}
