import Fuse from 'fuse.js';
import rawResourceIndex from '../../data/indices/fuse/resources.json';
import rawTagIndex from '../../data/indices/fuse/tags.json';
import AllTheResources from '../../data/resources';
import AllTheTags from '../../data/tags';
import { Resource } from '../models/resource';
import type { Tag } from '../models/tag';
import type { InternalSearchEngine } from './search-engine';


export interface IndexableResource {
  readonly title: string;
  readonly description: string;
  readonly url: string;
  readonly tags: ReadonlyArray<Tag>;
  readonly id: string;
}

export function FuseTagSearchEngine(): InternalSearchEngine<Tag> {
  const tagIndex = Fuse.parseIndex(rawTagIndex);
  const engine = new Fuse<Tag>(
    Array.from(AllTheTags),
    //  https://fusejs.io/api/options.html
    {
      // https://fusejs.io/api/options.html#threshold
      threshold: 0.3,
    },
    tagIndex,
  );

  return {
    search: (term) => engine.search(term).map((result) => result.item),
    setDocs: (docs) => engine.setCollection(docs),
  };
}

function mapIndexableResourceToResource(ir: IndexableResource): Resource {
  return {
    description: ir.description,
    tags: new Set(ir.tags),
    title: ir.title,
    url: ir.url,
    id: ir.id,
  };
}

export function mapResourceToIndexableResource(r: Resource): IndexableResource {
  return {
    description: r.description,
    title: r.title,
    url: r.url,
    tags: Array.from(r.tags),
    id: r.id,
  };
}

export const resourceSearchKeys: Fuse.FuseOptionKeyObject[] = [
  { name: 'title', weight: 4 },
  { name: 'description', weight: 3 },
  { name: 'url', weight: 2 },
  { name: 'tags', weight: 1 },
];

export function FuseResourceSearchEngine(): InternalSearchEngine<Resource> {
  const resourceIndex = Fuse.parseIndex(rawResourceIndex);
  const engine = new Fuse<IndexableResource>(
    Array
      .from(AllTheResources)
      .map(mapResourceToIndexableResource),
    //  https://fusejs.io/api/options.html
    {
      // https://fusejs.io/api/options.html#threshold
      threshold: 0.4,
      keys: resourceSearchKeys,
    },
    resourceIndex,
  );

  return {
    search: (term) => engine
      .search(term)
      .map((result) => result.item)
      .map(mapIndexableResourceToResource),
    setDocs: (docs) => engine.setCollection(docs.map(mapResourceToIndexableResource)),
  };
}
