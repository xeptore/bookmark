import { copyResource } from '../models/resource';
import type { Resource } from '../models/resource';
import { FuseResourceSearchEngine } from './fuse';
import type { SearchEngine, InternalSearchEngine } from './search-engine';


export default function ResourcesSearchEngine(): SearchEngine<Resource> {
  const engines: InternalSearchEngine<Resource>[] = [FuseResourceSearchEngine()];

  function searchInAllEngines(term: string): ReadonlySet<Resource> {
    const output = new Map<string, Resource>();
    const searchResults = engines.map((engine) => engine.search(term)).flat();

    for (const resultItem of searchResults) {
      if (!output.has(resultItem.id)) {
        output.set(resultItem.id, copyResource(resultItem));
      }
    }

    return new Set(output.values());
  }

  return {
    search: (term) => searchInAllEngines(term),
    setDocs: (docs) => engines.forEach((engine) => engine.setDocs(docs)),
  };
}
