import { copyTag, Tag } from '../models/tag';
import { FuseTagSearchEngine } from './fuse';
import type { SearchEngine, InternalSearchEngine } from './search-engine';


export default function TagsSearchEngine(): SearchEngine<Tag> {
  const engines: InternalSearchEngine<Tag>[] = [FuseTagSearchEngine()];

  function searchInAllEngines(term: string): ReadonlySet<Tag> {
    return new Set(engines.map((engine) => engine.search(term)).flat().map(copyTag));
  }

  return {
    search: (term) => searchInAllEngines(term),
    setDocs: (docs) => engines.forEach((engine) => engine.setDocs(docs)),
  };
}
