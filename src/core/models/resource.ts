import { copyString } from '../util';
import { copyTag } from './tag';
import type { Tag } from './tag';


export interface InputResource {
  readonly tags: ReadonlySet<Tag>;
  readonly title: string;
  readonly description: string;
  readonly url: string;
}

export interface Resource extends InputResource {
  readonly id: string;
}

function copyTags(tags: ReadonlySet<Tag>): ReadonlySet<Tag> {
  return new Set(Array.from(tags.values(), copyTag));
}

export function copyResource(old: Resource): Resource {
  return {
    tags: copyTags(old.tags),
    title: copyString(old.title),
    description: copyString(old.description),
    url: copyString(old.url),
    id: copyString(old.id),
  };
}

export function filterResourcesWithTags(
  resources: ReadonlySet<Resource>,
  tags: ReadonlySet<Tag>,
): ReadonlySet<Resource> {
  const output = new Set<Resource>();

  for (const resource of resources.values()) {
    if (Array.from(tags.values()).every((t) => resource.tags.has(t))) {
      output.add(copyResource(resource));
    }
  }

  return output;
}
