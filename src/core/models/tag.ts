import { copyString } from '../util';


export enum Tag {
  DOCKER = 'Docker',
  CONTAINERIZATION = 'Containerization',
  VIRTUALIZATION = 'Virtualization',
  BEST_PRACTICE = 'Best Practice',
  SPECIFICATION = 'Specification',
  ARCHITECTURE = 'Architecture',
  JAVASCRIPT = 'Javascript',
  REACT = 'React',
  VUE = 'Vue',
  ANGULAR = 'Angular',
  SVELTE = 'Svelte',
}

export function copyTag(t: Tag): Tag {
  return copyString(t) as Tag;
}

export function sortTags(unsorted: ReadonlySet<Tag>): ReadonlySet<Tag> {
  return new Set(Array.from(unsorted, copyTag).sort((a, b) => a.localeCompare(b)));
}

export function SortedTagsSet(tags: Iterable<Tag>): ReadonlySet<Tag> {
  return sortTags(new Set(tags));
}

export function excludeTag(toExcludeTag: Tag, tags: ReadonlySet<Tag>): ReadonlySet<Tag> {
  const output = Array.from<Tag>({ length: tags.size - 1 });

  let i = 0;
  for (const tag of tags) {
    if (tag !== toExcludeTag) {
      output[i++] = copyTag(tag);
    }
  }

  return new Set(output);
}

export function includeTag(toIncludeTag: Tag, tags: ReadonlySet<Tag>): ReadonlySet<Tag> {
  return new Set(Array.from(tags, copyTag).concat(copyTag(toIncludeTag)));
}

export function diffTags(
  minuend: ReadonlySet<Tag>,
  subtrahend: ReadonlySet<Tag>,
): ReadonlySet<Tag> {
  const output = new Set<Tag>();

  for (const tag of minuend) {
    if (!subtrahend.has(tag)) {
      output.add(copyTag(tag));
    }
  }

  return output;
}
