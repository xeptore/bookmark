import type { JSXElement, Accessor, Component } from 'solid-js';
import { For } from 'solid-js/web';
import type { Tag } from '../core/models/tag';
import type { TagItemProps } from './TagItem';


export interface TagListProps {
  readonly tags: Accessor<ReadonlySet<Tag>>;
  readonly TagItem: Component<TagItemProps>;
  readonly onclick: (tag: Tag) => void;
}

export default function TagList(props: TagListProps): JSXElement {
  return (
    <ul>
      <For each={Array.from(props.tags())}>
        {(tag) => <props.TagItem text={tag} onclick={() => props.onclick(tag)} />}
      </For>
    </ul>
  );
}
