import './App.sass';
import type { JSXElement } from 'solid-js';
import {
  createSignal,
  createMemo,
  createEffect,
  on,
} from 'solid-js';
import { For } from 'solid-js/web';
import { filterResourcesWithTags } from '../core/models/resource';
import type { Resource } from '../core/models/resource';
import {
  Tag,
  diffTags,
  excludeTag,
  includeTag,
  copyTag,
} from '../core/models/tag';
import type { SearchEngine } from '../core/search/search-engine';
import AllTheResources from '../data/resources.gen';
import AllTheTags from '../data/tags';
import TagItemSelected from './TagItemSelected';
import TagItemUnselected from './TagItemUnselected';
import TagList from './TagList';


function intersectFilteredAndUnselectedTags(
  filteredTags: ReadonlySet<Tag>,
  unselectedTags: ReadonlySet<Tag>,
): ReadonlySet<Tag> {
  const output = Array.from<Tag>({ length: Math.min(filteredTags.size, unselectedTags.size) });

  let idx = 0;
  for (const tag of unselectedTags) {
    if (filteredTags.has(tag)) {
      output[idx++] = copyTag(tag);
    }
  }

  return new Set(output);
}

export interface AppProps {
  readonly tagsSearchEngine: SearchEngine<Tag>;
  readonly resourcesSearchEngine: SearchEngine<Resource>;
}

export default function App(props: AppProps): JSXElement {
  const [selectedTags, setSelectedTags] = createSignal<ReadonlySet<Tag>>(
    new Set(),
    { name: 'SelectedTags' },
  );

  const [tagSearchValue, setTagSearchValue] = createSignal('');
  const [resourceSearchValue, setResourceSearchValue] = createSignal('');

  const unselectedTags = createMemo(
    () => diffTags(AllTheTags, selectedTags()),
    undefined,
    { name: 'UnselectedTags' },
  );

  const filteredResources = createMemo(
    () => filterResourcesWithTags(new Set(AllTheResources.values()), selectedTags()),
    undefined,
    { name: 'FilteredResources' },
  );

  const [
    searchedFilteredResources,
    setSearchedFilteredResources,
  ] = createSignal(filteredResources());

  const [filteredUnselectedTags, setFilteredUnselectedTags] = createSignal(unselectedTags());

  function searchInTags(value: string): void {
    if (value === '') {
      setFilteredUnselectedTags(unselectedTags());
      return;
    }
    const result = props.tagsSearchEngine.search(value);
    setFilteredUnselectedTags(() => result);
  }

  function searchInResources(value: string): void {
    if (value === '') {
      setSearchedFilteredResources(filteredResources());
      return;
    }
    const result = props.resourcesSearchEngine.search(value);
    setSearchedFilteredResources(() => result);
  }

  createEffect(on(tagSearchValue, searchInTags));
  createEffect(on(resourceSearchValue, searchInResources));

  createEffect(
    on(
      unselectedTags,
      (v) => {
        setFilteredUnselectedTags(
          (fut) => intersectFilteredAndUnselectedTags(fut, v),
        );

        props.tagsSearchEngine.setDocs(Array.from(v));
        searchInTags(tagSearchValue());
      },
    ),
  );


  createEffect(on(
    filteredResources,
    ((newFilteredResources) => {
      props.resourcesSearchEngine.setDocs(Array.from(newFilteredResources));
      searchInResources(resourceSearchValue());
    }),
    { defer: true },
  ));

  function handleSelectTag(tag: Tag): void {
    setSelectedTags((tags) => includeTag(tag, tags));
  }

  function handleUnselectTag(tag: Tag): void {
    setSelectedTags((tags) => excludeTag(tag, tags));
  }

  function handleResetSelectedTags(): void {
    setSelectedTags(() => new Set<Tag>());
  }


  return (
    <main>
      <input
        type="text"
        name="tag-search"
        id="tag-search"
        oninput={(e) => setTagSearchValue(e.currentTarget.value)}
        value={tagSearchValue()}
      />

      <TagList
        TagItem={TagItemUnselected}
        tags={filteredUnselectedTags}
        onclick={(tag) => handleSelectTag(tag)}
      />

      <hr />

      <button type="reset" onclick={() => handleResetSelectedTags()}>
        reset
      </button>
      <TagList
        TagItem={TagItemSelected}
        tags={selectedTags}
        onclick={(tag) => handleUnselectTag(tag)}
      />

      <hr />

      <input
        type="text"
        name="resource-search"
        id="resource-search"
        oninput={(e) => setResourceSearchValue(e.currentTarget.value)}
        value={resourceSearchValue()}
      />

      <ul class="cards">
        <For
          each={Array.from(searchedFilteredResources())}
          fallback={<div>Loading...</div>}
        >
          {(r) => (
            <li class="card">
              <h2 class="title">{r.title}</h2>
              <p class="description">{r.description}</p>
              <ul class="tags">
                <For each={Array.from(r.tags.values())}>
                  {function tag(t) {
                    const selected = selectedTags().has(t);
                    return (
                      <li class="tag" classList={{ selected }}>
                        <button
                          onclick={() => (selected ? handleUnselectTag(t) : handleSelectTag(t))
                          }
                          type="button"
                        >
                          {t}
                        </button>
                      </li>
                    );
                  }}
                </For>
              </ul>
              <a class="url" href={r.url}>
                <span>{r.url}</span>
              </a>
            </li>
          )}
        </For>
      </ul>
    </main>
  );
}
