import type { JSXElement } from 'solid-js';
import style from './TagItemSelected.module.sass';
import type { TagItemProps } from './TagItem';
import { Wrapper } from './TagItem';


export default function TagItemSelected(props: TagItemProps): JSXElement {
  return (
    <Wrapper>
      <span class={style.text}>{props.text}</span>
      <button
        class={style.button}
        type="button"
        onclick={() => props.onclick()}
      >
        <span>&Cross;</span>
      </button>
    </Wrapper>
  );
}
