import type { JSXElement } from 'solid-js';
import style from './TagItemUnselected.module.sass';
import type { TagItemProps } from './TagItem';
import { Wrapper } from './TagItem';


export default function TagItemUnselected(props: TagItemProps): JSXElement {
  return (
    <Wrapper>
      <button class={style.button} type="button" onclick={() => props.onclick()}>{props.text}</button>
    </Wrapper>
  );
}
