import type { JSXElement, PropsWithChildren } from 'solid-js';
import style from './TagItem.module.sass';


export function Wrapper(props: PropsWithChildren): JSXElement {
  return (
    <li class={style.wrapper}>
      {props.children}
    </li>
  );
}

export interface TagItemProps {
  readonly text: string;
  readonly onclick: () => void;
}
