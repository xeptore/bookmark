import { render } from 'solid-js/web';
import './index.sass';
import ResourcesSearchEngine from '../core/search/resources-engine';
import TagsSearchEngine from '../core/search/tags-engine';
import App from './App';


const tagsSearchEngine = TagsSearchEngine();
const resourcesSearchEngine = ResourcesSearchEngine();
render(() => <App tagsSearchEngine={tagsSearchEngine} resourcesSearchEngine={resourcesSearchEngine} />, document.getElementById('root'));
