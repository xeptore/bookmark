import fs from 'fs';
import path from 'path/posix';
import Fuse from 'fuse.js';
import { mapResourceToIndexableResource, resourceSearchKeys } from '../core/search/fuse';
import AllTheResources from '../data/resources';
import AllTheTags from '../data/tags';


function generateFuseIndices() {
  const fuseIndicesDirectoryPath = path.resolve(
    __dirname,
    '..',
    'data',
    'indices',
    'fuse',
  );

  const tagsIndices = Fuse.createIndex([], Array.from(AllTheTags));
  fs.writeFileSync(
    path.join(fuseIndicesDirectoryPath, 'tags.json'),
    JSON.stringify(tagsIndices.toJSON()),
    { encoding: 'utf-8' },
  );

  const resourceIndices = Fuse.createIndex(
    resourceSearchKeys,
    Array.from(AllTheResources).map(mapResourceToIndexableResource),
  );
  fs.writeFileSync(
    path.join(fuseIndicesDirectoryPath, 'resources.json'),
    JSON.stringify(resourceIndices.toJSON()),
    { encoding: 'utf-8' },
  );
}

if (require.main === module) {
  generateFuseIndices();
}
