import fs from 'fs';
import path from 'path/posix';
// eslint-disable-next-line import/no-extraneous-dependencies
import { nanoid } from 'nanoid';
// eslint-disable-next-line import/no-extraneous-dependencies
import ts from 'typescript';
import type {
  ObjectLiteralExpression,
  ArrayLiteralExpression,
  VariableStatement,
} from 'typescript';


const sourceFileName = path.resolve(__dirname, '..', 'data', 'resources.ts');
const sourceFile = ts.createSourceFile(
  sourceFileName,
  fs.readFileSync(sourceFileName, { encoding: 'utf8' }),
  ts.ScriptTarget.Latest,
  false,
  ts.ScriptKind.TS,
);

const importStatements = sourceFile.statements
  .filter((stmt) => stmt.kind === ts.SyntaxKind.ImportDeclaration);

const resourceVars = sourceFile.statements
  .filter((stmt) => stmt.kind === ts.SyntaxKind.VariableStatement)[0] as VariableStatement;

const resourceItems = resourceVars
  .declarationList
  .declarations
  .map(
    (decl) => (decl.initializer as ArrayLiteralExpression)!.elements
      .map((node) => node as ObjectLiteralExpression),
  )[0];

const resourceImport = ts.factory.createImportDeclaration(
  undefined,
  undefined,
  ts.factory.createImportClause(
    true,
    undefined,
    ts.factory.createNamedImports([
      ts.factory.createImportSpecifier(
        undefined,
        ts.factory.createIdentifier('Resource'),
      ),
    ]),
  ),
  ts.factory.createStringLiteral('../core/models/resource', true),
);

const vars = ts.factory.createVariableStatement(
  undefined,
  ts.factory.createVariableDeclarationList([
    ts.factory.createVariableDeclaration(
      ts.factory.createIdentifier('AllTheResources'),
      undefined,
      ts.factory.createTypeReferenceNode(
        ts.factory.createIdentifier('Map'),
        [
          ts.factory.createToken(ts.SyntaxKind.StringKeyword),
          ts.factory.createTypeReferenceNode(
            ts.factory.createIdentifier('Resource'),
          ),
        ],
      ),
      ts.factory.createNewExpression(
        ts.factory.createIdentifier('Map'),
        [
          ts.factory.createToken(ts.SyntaxKind.StringKeyword),
          ts.factory.createTypeReferenceNode(
            ts.factory.createIdentifier('Resource'),
          ),
        ],
        [
          ts.factory.createArrayLiteralExpression(
            resourceItems.map((r) => {
              const id = nanoid(128);
              return ts.factory.createArrayLiteralExpression([
                ts.factory.createStringLiteral(id, true),
                ts.factory.createObjectLiteralExpression(
                  r.properties.concat(
                    ts.factory.createPropertyAssignment(
                      'id',
                      ts.factory.createStringLiteral(id, true),
                    ),
                  ),
                  true,
                ),
              ], true);
            }),
            true,
          ),
        ],
      ),
    ),
  ], ts.NodeFlags.Const),
);

const exportedAssignment = ts.factory.createExportAssignment(
  undefined,
  undefined,
  false,
  ts.factory.createIdentifier('AllTheResources'),
);

const resultFile = ts.createSourceFile(
  'someFileName.ts',
  '',
  ts.ScriptTarget.Latest,
  false,
  ts.ScriptKind.TS,
);

const printer = ts.createPrinter({
  newLine: ts.NewLineKind.LineFeed,
  omitTrailingSemicolon: false,
  removeComments: false,
});

const out = [
  importStatements[0],
  importStatements[1],
  resourceImport,
]
  .map((n) => printer.printNode(ts.EmitHint.Unspecified, n, resultFile))
  .join('\n')
  .concat(
    '\n',
    '\n',
    [
      vars,
      exportedAssignment,
    ].map((n) => printer.printNode(ts.EmitHint.Unspecified, n, resultFile)).join('\n'),
  );

fs.writeFileSync(
  path.join(__dirname, '..', 'data', 'resources.gen.ts'),
  out,
  { encoding: 'utf8' },
);
