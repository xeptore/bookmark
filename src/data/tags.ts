import { SortedTagsSet, Tag } from '../core/models/tag';


const AllTheTags: ReadonlySet<Tag> = SortedTagsSet(Object.values(Tag));

export default AllTheTags;
