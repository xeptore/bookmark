import type { InputResource } from '../core/models/resource';
import { SortedTagsSet, Tag } from '../core/models/tag';


const inputResources: InputResource[] = [
  {
    description: 'Viverra mus hac eros magnis consequat egestas eget amet vestibulum habitant eget mollis lectus',
    tags: SortedTagsSet([Tag.ANGULAR, Tag.BEST_PRACTICE]),
    title: 'Angular Best Practices',
    url: 'https://angular.io/best_practices',
  },
  {
    description: 'sapien a consectetuer iaculis elementum eros molestie risus',
    tags: SortedTagsSet([Tag.DOCKER, Tag.SPECIFICATION]),
    title: 'Docker compose spec',
    url: 'https://angular.io/best_practices',
  },
  {
    description: 'vestibulum neque posuere tempor velit hendrerit penatibus enim euismod, posuere posuere. Ridiculus platea congue massa, et lectus ultricies orci sapien',
    tags: SortedTagsSet([Tag.SVELTE, Tag.ARCHITECTURE]),
    title: 'Svelte architecture',
    url: 'https://svelte.dev/architecture',
  },
  {
    description: 'Ultrices gravida parturient. Viverra mus hac eros magnis consequat egestas eget amet vestibulum habitant eget mollis lectus, lacinia viverra placerat per mus consequat aliquam. Quisque luctus',
    tags: SortedTagsSet([Tag.REACT, Tag.ARCHITECTURE, Tag.VUE]),
    title: 'React vs. Vue',
    url: 'https://comparision.dev/react_vue',
  },
  {
    description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod',
    tags: SortedTagsSet([Tag.CONTAINERIZATION, Tag.SPECIFICATION]),
    title: 'Lorem Ipsum Text Generator',
    url: 'https://angular.io/best_practices',
  },
  {
    description: 'Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
    tags: SortedTagsSet([Tag.REACT, Tag.SVELTE]),
    title: 'Why do you use',
    url: 'https://angular.io/best_practices',
  },
  {
    description: 'nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate',
    tags: SortedTagsSet([Tag.JAVASCRIPT, Tag.DOCKER]),
    title: 'This expedient serves to get an idea',
    url: 'https://svelte.dev/architecture',
  },
  {
    description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium',
    tags: SortedTagsSet([Tag.DOCKER, Tag.ARCHITECTURE, Tag.SPECIFICATION]),
    title: 'Lorem ipsum contains the typefaces more in use',
    url: 'https://comparision.dev/react_vue',
  },
];

export default inputResources;
