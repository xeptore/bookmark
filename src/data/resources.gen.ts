import type { InputResource, Resource } from '../core/models/resource';
import { SortedTagsSet, Tag } from '../core/models/tag';


const AllTheResources: Map<string, Resource> = new Map<string, Resource>([
  [
    'gajKGlhrJhW3Y3KuVylWX9x5IlPNGfR6f_3wZ2BqXRxnxfA7AGAKySC-s-93q7yoyisqFaYnkD1hNstLRdt-KYcRJqsVnpRZB0U4aAHAQadzEgiNUxwi19BWRAhtn2fE',
    {
      description: 'Viverra mus hac eros magnis consequat egestas eget amet vestibulum habitant eget mollis lectus',
      tags: SortedTagsSet([Tag.ANGULAR, Tag.BEST_PRACTICE]),
      title: 'Angular Best Practices',
      url: 'https://angular.io/best_practices',
      id: 'gajKGlhrJhW3Y3KuVylWX9x5IlPNGfR6f_3wZ2BqXRxnxfA7AGAKySC-s-93q7yoyisqFaYnkD1hNstLRdt-KYcRJqsVnpRZB0U4aAHAQadzEgiNUxwi19BWRAhtn2fE',
    },
  ],
  [
    '45DjlYJ64aTTfv3CS4TIkQ-wlWZfALUr1P25rhZfUyih2jtcwwTgAozvS3lFGXLQCvl3men_laO5MTMSmGg4IDmJl8sycyRiwhOaA5QjC_h64i6RdDUMOs91wdGqWqgj',
    {
      description: 'sapien a consectetuer iaculis elementum eros molestie risus',
      tags: SortedTagsSet([Tag.DOCKER, Tag.SPECIFICATION]),
      title: 'Docker compose spec',
      url: 'https://angular.io/best_practices',
      id: '45DjlYJ64aTTfv3CS4TIkQ-wlWZfALUr1P25rhZfUyih2jtcwwTgAozvS3lFGXLQCvl3men_laO5MTMSmGg4IDmJl8sycyRiwhOaA5QjC_h64i6RdDUMOs91wdGqWqgj',
    },
  ],
  [
    'TjVZ182OLJ0odpLoyosS08nlHrJlwwQ4LtOcGIR5JtpO-Birx3Hg6JnLbtgSTYOTl94iUfcS8_4KIUshePBJl681eeMlpvZKvHthcrsa_Cp67AzRxLDLp8ZoGknUTyKC',
    {
      description: 'vestibulum neque posuere tempor velit hendrerit penatibus enim euismod, posuere posuere. Ridiculus platea congue massa, et lectus ultricies orci sapien',
      tags: SortedTagsSet([Tag.SVELTE, Tag.ARCHITECTURE]),
      title: 'Svelte architecture',
      url: 'https://svelte.dev/architecture',
      id: 'TjVZ182OLJ0odpLoyosS08nlHrJlwwQ4LtOcGIR5JtpO-Birx3Hg6JnLbtgSTYOTl94iUfcS8_4KIUshePBJl681eeMlpvZKvHthcrsa_Cp67AzRxLDLp8ZoGknUTyKC',
    },
  ],
  [
    'tmC9kkaj7xJWbC0oRZCyHzqHcHIuT3lNCNvZqapHxKl6S-xN0OvaCF835X7xLHia73b4wpMLLZjPoP9FrxIDMfeWvx0ipm1CCj95hWqjUD3m3Hr2FqWtdFbu_hCpEIaG',
    {
      description: 'Ultrices gravida parturient. Viverra mus hac eros magnis consequat egestas eget amet vestibulum habitant eget mollis lectus, lacinia viverra placerat per mus consequat aliquam. Quisque luctus',
      tags: SortedTagsSet([Tag.REACT, Tag.ARCHITECTURE, Tag.VUE]),
      title: 'React vs. Vue',
      url: 'https://comparision.dev/react_vue',
      id: 'tmC9kkaj7xJWbC0oRZCyHzqHcHIuT3lNCNvZqapHxKl6S-xN0OvaCF835X7xLHia73b4wpMLLZjPoP9FrxIDMfeWvx0ipm1CCj95hWqjUD3m3Hr2FqWtdFbu_hCpEIaG',
    },
  ],
  [
    'gzyxkxiXmA8GMjiJdBHytIAl2uyPYvctMCW9fbhu-rD8AmZIdx8wrpMOKET4nVWEUTIdxneMEE78PKC-9zg9Ry7GFnNfolxrec87dSXYRptwT5GKlSyZsfaqu73zQa0M',
    {
      description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod',
      tags: SortedTagsSet([Tag.CONTAINERIZATION, Tag.SPECIFICATION]),
      title: 'Lorem Ipsum Text Generator',
      url: 'https://angular.io/best_practices',
      id: 'gzyxkxiXmA8GMjiJdBHytIAl2uyPYvctMCW9fbhu-rD8AmZIdx8wrpMOKET4nVWEUTIdxneMEE78PKC-9zg9Ry7GFnNfolxrec87dSXYRptwT5GKlSyZsfaqu73zQa0M',
    },
  ],
  [
    'GjMDpcDj2VjZb0QJ2p32qynMYNlGow27DTE0IERrpqvYi0GcLV2Ld0FmSn_9FXZaG664PEL658-CYl7x6H1cmQjqzTwjKcDuY0aI-4qi_bM2d2WZZmwEsB8L0tecAbUj',
    {
      description: 'Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
      tags: SortedTagsSet([Tag.REACT, Tag.SVELTE]),
      title: 'Why do you use',
      url: 'https://angular.io/best_practices',
      id: 'GjMDpcDj2VjZb0QJ2p32qynMYNlGow27DTE0IERrpqvYi0GcLV2Ld0FmSn_9FXZaG664PEL658-CYl7x6H1cmQjqzTwjKcDuY0aI-4qi_bM2d2WZZmwEsB8L0tecAbUj',
    },
  ],
  [
    'YMblBO4NSIvNWq3pNt9MK7_DZbAuNaFAWYUjvUQaupEajPypchuFmopLAoTz7mFRnl8-iKhSnTnOFarF03XgOS0e3m7Bp_QhRcEWocbQr6PoG4NQatOE1O3CpTTqmzze',
    {
      description: 'nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate',
      tags: SortedTagsSet([Tag.JAVASCRIPT, Tag.DOCKER]),
      title: 'This expedient serves to get an idea',
      url: 'https://svelte.dev/architecture',
      id: 'YMblBO4NSIvNWq3pNt9MK7_DZbAuNaFAWYUjvUQaupEajPypchuFmopLAoTz7mFRnl8-iKhSnTnOFarF03XgOS0e3m7Bp_QhRcEWocbQr6PoG4NQatOE1O3CpTTqmzze',
    },
  ],
  [
    'c99iUCXvMSumUiQVaFF3KDXG7lM7IiHflfRhI1Wrrw9zBrRY7xmdjSGzyFi6Z-7FjcvX5DQQg2th-kmNmmDSOPJbmr-9JKbL4ilGnGYqsHnQirrgxEerf9yY9ZJ-dwLL',
    {
      description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium',
      tags: SortedTagsSet([Tag.DOCKER, Tag.ARCHITECTURE, Tag.SPECIFICATION]),
      title: 'Lorem ipsum contains the typefaces more in use',
      url: 'https://comparision.dev/react_vue',
      id: 'c99iUCXvMSumUiQVaFF3KDXG7lM7IiHflfRhI1Wrrw9zBrRY7xmdjSGzyFi6Z-7FjcvX5DQQg2th-kmNmmDSOPJbmr-9JKbL4ilGnGYqsHnQirrgxEerf9yY9ZJ-dwLL',
    },
  ],
]);
export default AllTheResources;
